class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :authenticate_user
  include ApplicationHelper

  

  private

  def authenticate_user
    if current_user.blank?

        if restricted_action?

       redirect_to log_in_path
      end
    end

  end

  def restricted_action?
    if controller_name == "sessions"
      ["new","create"].exclude? action_name
    elsif controller_name == "users"
      ["new","create"].exclude? action_name
    else
      true
    end
  end


end
