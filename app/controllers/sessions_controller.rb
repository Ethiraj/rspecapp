class SessionsController < ApplicationController
  def new
  end

  def create
    name = params[:session][:name]
    password = params[:session][:password]
    if  User.authenticate(name,password)
      session[:user_id] = User.find_by_name(name).id
      redirect_to users_path
    else
      session[:user_id] = nil
      redirect_to log_in_path
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to log_in_path
  end
end
