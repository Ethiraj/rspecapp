class User < ActiveRecord::Base
  validates_presence_of :name, :password
  validates_uniqueness_of :name

  private

  def self.authenticate(name,password)
    User.exists?(name: name,password: password)
  end
end
