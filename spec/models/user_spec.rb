# spec/models/user.rb
require 'rails_helper'

describe User do
  it "has a valid factory" do
   FactoryGirl.create(:user).should be_valid
  end

  it "is invalid without a name" do
    FactoryGirl.build(:user, name: nil).should_not be_valid
  end

  it "is invalid without a password" do
    FactoryGirl.build(:user, password: nil).should_not be_valid
  end

  it "does not allow duplicate names" do
    user = FactoryGirl.create(:user)
    FactoryGirl.build(:user, name: user.name).should_not be_valid
  end

  describe "should be authenticated  " do
    before :each do
    @newuser = FactoryGirl.create(:user, name: 'ethiraj9',password: 'ethiraj9')
    end

    context "Matching credentails" do
      it "authenticates user with matching credentails"  do
        User.authenticate('ethiraj9','ethiraj9').should be_truthy
      end
    end

    context "Non Matching credentails" do
      it "does not authenticate user with  non-matching credentails"  do
        User.authenticate('ethiraj91','ethiraj91').should_not be_truthy
      end
    end
  end

end
