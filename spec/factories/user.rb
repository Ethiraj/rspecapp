# spec/factories/user.rb
require 'faker'
FactoryGirl.define do
  factory :user do |f|
    f.name   { Faker::Name.name }
    f.password { Faker::Internet.password }
  end

  factory :invalid_user, parent: :user do |f|
  f.name nil
  end

end
