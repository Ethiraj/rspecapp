# spec/models/user.rb
require 'rails_helper'

feature 'User Sign up the form ' do

  let :user do
    FactoryGirl.build(:user)
  end
  scenario 'they create their account' do
    visit sign_up_path

    fill_in 'Name', with: user.name
    fill_in 'Password', with: user.password
    click_button 'Sign Up'
    page.should have_content user.name
  end
end
