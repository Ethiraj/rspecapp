# spec/acceptance/sessions.rb
require 'rails_helper'

feature 'User Sign in the form ' do

  let :user do
    FactoryGirl.create(:user)
  end
  scenario 'they login their account' do
    visit log_in_path
    fill_in 'Name', with: user.name
    fill_in 'Password', with: user.password
    click_button 'Sign In'
    page.should have_content user.name
  end
  scenario 'they logout their account' do
    sign_in_with user
    visit users_path
    click_link 'Log out'
    page.should have_content "log in"
  end
end
