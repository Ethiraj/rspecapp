# spec/controllers/users_controller_spec.rb
require 'rails_helper'

describe UsersController do
  describe "GET #index" do
    let :user do
      FactoryGirl.create(:user)
    end
    it "populates an array of users" do
      sign_in user
      get :index
      expect(assigns[:users]).to eq([user])
    end
    it "renders the :index view" do
      sign_in user
      get :index
      response.should render_template :index
    end
  end

  describe "GET #new" do
    it "assigns a new User to @user" do
      get :new
      expect(assigns(:user)).to be_a_new(User)
    end

    it "renders the :new template" do
      get :new
      response.should render_template :new
    end
  end

  describe "POST #create" do
    context "with valid attributes" do
      it "saves the new user in the database" do
        expect{post :create, user: FactoryGirl.attributes_for(:user)
        }.to change(User,:count).by(1)
      end
      it "redirects to the users index page" do
        post :create, user: FactoryGirl.attributes_for(:user)
        response.should redirect_to users_path
      end
    end

    context "with invalid attributes" do
      it "does not save the new user in the database" do
        expect{post :create, user: FactoryGirl.attributes_for(:invalid_user)
        }.to_not change(User,:count)
      end
      it "re-renders the :new template" do
        post :create, user: FactoryGirl.attributes_for(:invalid_user)
        response.should render_template :new
      end
    end
  end
end
