# spec/controllers/users_controller_spec.rb
require 'rails_helper'

describe SessionsController do
  describe "GET #new" do
    it "should render login page" do
      get :new
      response.should render_template :new
    end
  end

  describe "DELETE #destroy" do
    let :user do
      FactoryGirl.create(:user)
    end
    it "should clear user session id" do
      sign_in user
      delete :destroy
      expect(session[:user_id]).to eq nil
    end
    it "should redirect to log in page" do
      sign_in user
      delete :destroy
      response.should redirect_to log_in_path
    end
  end

  describe "POST #create" do
    let :user do
      FactoryGirl.create(:user)
    end
    let :credentials do
      {session: { :name => user.name, :password => user.password }}
    end
    it "should assign user id to session" do
      post :create, credentials
      session[:user_id].should == user.id
    end
    it "should redirect to home page " do
      post :create, credentials
      response.should redirect_to users_path
    end
  end
end
