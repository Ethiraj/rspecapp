module SpecTestHelper
  def sign_in(user)
    session[:user_id] = User.find_by_name(user.name).id if  User.authenticate(user.name,user.password)
  end

  def sign_in_with(user)
    visit log_in_path
    fill_in 'Name', with: user.name
    fill_in 'Password', with: user.password
    click_button 'Sign In'
  end
end
